# Daily Report (2023/07/21)

## O

### Flyway

##### This is the first time I have come into contact with the Flyway technology. It gives me the feeling that it can significantly improve the efficiency of database management, make database migration more controllable and reliable, and help team members work better together. Ensure the consistency of the database structure.

### Cloud Native

##### This afternoon, the three groups had a knowledge sharing session on microservices, containers and CI/CD. These three technologies are interdependent. Under the architecture of containers and microservices, CI/CD can be implemented more effectively. Container technology provides a consistent operating environment, making it easier to deploy and run microservices in different environments. CI/CD tools can integrate with container technology to automate the construction, testing, and deployment of microservices to achieve rapid iteration and delivery.

##### 

## R

##### meaningful

## I

##### The most meaningful activity is Cloud Native

## D

##### I will spend some time to understand it after class.

###### Use Mapper in real projects to handle requests and responses.