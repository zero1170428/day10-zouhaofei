package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.employee.EmployeeRequest;
import com.afs.restapi.service.dto.employee.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    private static Employee getEmployeeZhangsan() {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestZhangsan() {
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("zhangsan");
        employeeRequest.setAge(22);
        employeeRequest.setGender("Male");
        return employeeRequest;
    }

    private static Employee getEmployeeSusan() {
        Employee employee = new Employee();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static Employee getEmployeeLisi() {
        Employee employee = new Employee();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }

    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_return_employeeResponse_when_updateEmployee_given_employeeRequest() throws Exception {
        Employee previousEmployee = new Employee(1L, "zhangsan", 22, "Male", 1000);
        Employee returnEmployee = employeeJPARepository.save(previousEmployee);

        Employee employeeUpdateRequest = new Employee(returnEmployee.getId(), "zhangsan", 24, "Male", 2000);
        EmployeeResponse employeeResponse = EmployeeMapper.toResponse(employeeUpdateRequest);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);
        mockMvc.perform(put("/employees/{id}", returnEmployee.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employeeResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeResponse.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeResponse.getGender()));

        Optional<Employee> optionalEmployee = employeeJPARepository.findById(returnEmployee.getId());
        assertTrue(optionalEmployee.isPresent());
        Employee updatedEmployee = optionalEmployee.get();
        Assertions.assertEquals(employeeUpdateRequest.getAge(), updatedEmployee.getAge());
        Assertions.assertEquals(employeeUpdateRequest.getSalary(), updatedEmployee.getSalary());
        Assertions.assertEquals(returnEmployee.getId(), updatedEmployee.getId());
        Assertions.assertEquals(previousEmployee.getName(), updatedEmployee.getName());
        Assertions.assertEquals(previousEmployee.getGender(), updatedEmployee.getGender());
    }

    @Test
    void should_return_employeeResponse_when_createEmployee_given_employeeRequest() throws Exception {
        EmployeeRequest employee = getEmployeeRequestZhangsan();
        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(employee);
        mockMvc.perform(post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employee.getGender()));
    }

    @Test
    void should_return_all_employees_when_getAllEmployees() throws Exception {
        Employee employee = getEmployeeZhangsan();
        EmployeeResponse employeeResponse = EmployeeMapper.toResponse(employeeJPARepository.save(employee));
        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeResponse.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeResponse.getGender()));
    }

    @Test
    void should_retuen_employeeResponse_when_getEmployeeById_given_id() throws Exception {
        Employee employee = getEmployeeZhangsan();
        EmployeeResponse employeeResponse = EmployeeMapper.toResponse(employeeJPARepository.save(employee));

        mockMvc.perform(get("/employees/{id}", employeeResponse.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employeeResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(employeeResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(employeeResponse.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(employeeResponse.getGender()));
    }

    @Test
    void should_return_204_when_perform_delete_given_id() throws Exception {
        Employee employee = getEmployeeZhangsan();
        Employee returnEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(delete("/employees/{id}", returnEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(returnEmployee.getId()).isEmpty());
    }

    @Test
    void should_return_employeeResponses_when_perform_find_employee_by_gender() throws Exception {
        Employee employee = getEmployeeZhangsan();
        Employee returnEmployee = employeeJPARepository.save(employee);
        EmployeeResponse employeeResponse = EmployeeMapper.toResponse(employeeJPARepository.save(employee));

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeResponse.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeResponse.getGender()));
    }

    @Test
    void should_return_employeeResponses_when_perform_get_by_page_given_employees() throws Exception {
        Employee employeeZhangsan = getEmployeeZhangsan();
        Employee employeeSusan = getEmployeeSusan();
        Employee employeeLisi = getEmployeeLisi();
        EmployeeResponse employeeResponseZhangsan = EmployeeMapper.toResponse(employeeJPARepository.save(employeeZhangsan));
        EmployeeResponse employeeResponseSusan = EmployeeMapper.toResponse(employeeJPARepository.save(employeeSusan));
        EmployeeResponse employeeResponseLisi = EmployeeMapper.toResponse(employeeJPARepository.save(employeeLisi));

        mockMvc.perform(get("/employees")
                .param("page", "1")
                .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeResponseZhangsan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeResponseZhangsan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeResponseZhangsan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeResponseZhangsan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(employeeResponseSusan.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeResponseSusan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeResponseSusan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeResponseSusan.getGender()));
    }
}