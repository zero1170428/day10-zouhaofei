package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.dto.company.CompanyRequest;
import com.afs.restapi.service.dto.company.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private CompanyJPARepository companyJPARepository;
    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }

    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }

    private static CompanyRequest getCompanyRequest1() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("ABC");
        return companyRequest;
    }

    private static Company getCompany2() {
        Company company = new Company();
        company.setName("DEF");
        return company;
    }

    private static Company getCompany3() {
        Company company = new Company();
        company.setName("XYZ");
        return company;
    }

    @BeforeEach
    void setUp() {
        companyJPARepository.deleteAll();
        employeeJPARepository.deleteAll();
    }

    @Test
    void should_return_update_companyResponse_when_updateCompany_given_company() throws Exception {
        Company previousCompany = new Company(1L, "abc");
        Company returnPreviousCompany = companyJPARepository.save(previousCompany);

        CompanyResponse companyUpdateResponse = CompanyMapper.toResponse(new Company(returnPreviousCompany.getId(), "xyz"));
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateResponse);
        mockMvc.perform(put("/companies/{id}", returnPreviousCompany.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyUpdateResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(companyUpdateResponse.getId()));

        Optional<Company> optionalCompany = companyJPARepository.findById(returnPreviousCompany.getId());
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        Assertions.assertEquals(returnPreviousCompany.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateResponse.getName(), updatedCompany.getName());
    }

    @Test
    void should_return_void_when_deleteCompany_given_id() throws Exception {
        Company company = new Company(1L, "abc");
        CompanyResponse companyUpdateResponse = CompanyMapper.toResponse(companyJPARepository.save(company));
        Employee previousEmployee = new Employee(1L, "zhangsan", 22, "Male", 1000);
        previousEmployee.setCompanyId(companyUpdateResponse.getId());
        Employee returnEmployee = employeeJPARepository.save(previousEmployee);

        mockMvc.perform(delete("/companies/{id}", companyUpdateResponse.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(companyJPARepository.findById(companyUpdateResponse.getId()).isEmpty());
        assertTrue(employeeJPARepository.findById(returnEmployee.getId()).isEmpty());
//        Assertions.assertThrows(CompanyNotFoundException.class,() -> companyJPARepository.findById(companyUpdateResponse.getId()));
//        Assertions.assertThrows(EmployeeNotFoundException.class,() -> employeeJPARepository.findById(returnEmployee.getId()));
    }

    @Test
    void should_return_companyResponse_when_perform_createCompany_given_company() throws Exception {
        CompanyRequest company = getCompanyRequest1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(company);
        mockMvc.perform(post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()));
    }

    @Test
    void should_return_all_companies_with_companyResponse_when_perform_getAllCompanies() throws Exception {
        Company company = getCompany1();
        CompanyResponse companyResponse = CompanyMapper.toResponse(companyJPARepository.save(company));

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(companyResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(companyResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(0));
    }

    @Test
    void should_return_companyResponse_when_perform_getCompaniesByPage_given_page_and_size() throws Exception {
        Company company1 = getCompany1();
        Company company2 = getCompany2();
        Company company3 = getCompany3();
        CompanyResponse companyResponse1 = CompanyMapper.toResponse(companyJPARepository.save(company1));
        CompanyResponse companyResponse2 = CompanyMapper.toResponse(companyJPARepository.save(company2));
        companyJPARepository.save(company3);

        mockMvc.perform(get("/companies")
                .param("page", "1")
                .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(companyResponse1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(companyResponse1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(companyResponse2.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(companyResponse2.getName()));
    }

    @Test
    void should_return_companyResponse_when_perform_getCompanyById_given_id() throws Exception {
        Company company = getCompany1();
        CompanyResponse companyResponse = CompanyMapper.toResponse(companyJPARepository.save(company));
        company.setId(companyResponse.getId());
        Employee employee = getEmployee(company);
        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{id}", companyResponse.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(companyResponse.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyResponse.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(1));
    }

    @Test
    void should_return_employeeResponse_when_getEmployeesByCompanyId_given_id() throws Exception {
        Company company = getCompany1();
        Company returnCompany = companyJPARepository.save(company);
        Employee employee = getEmployee(company);
        Employee returnEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{companyId}/employees", returnCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(returnEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()));
    }
}