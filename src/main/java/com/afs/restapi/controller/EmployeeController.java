package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.service.dto.employee.EmployeeRequest;
import com.afs.restapi.service.dto.employee.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployees() {
        return employeeService.findAll().stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {

        return EmployeeMapper.toResponse(employeeService.findById(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public EmployeeResponse updateEmployee(@PathVariable Long id, @RequestBody Employee employee) {
        return EmployeeMapper.toResponse(employeeService.update(id, employee));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employeeService.findAllByGender(gender);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = employeeService.create(EmployeeMapper.toRntity(employeeRequest));
        return EmployeeMapper.toResponse(employee);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> findEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        return employeeService.findByPage(page, size);
    }

}
