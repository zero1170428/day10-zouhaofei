package com.afs.restapi.service.dto.employee;

public class EmployeeResponse {
    private String name;
    private Integer age;
    private String gender;
    private Long id;

    public EmployeeResponse() {
    }

    public EmployeeResponse(String name, Integer age, String gender, Long id) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
