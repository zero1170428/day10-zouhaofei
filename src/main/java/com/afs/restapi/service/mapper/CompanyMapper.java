package com.afs.restapi.service.mapper;


import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.company.CompanyRequest;
import com.afs.restapi.service.dto.company.CompanyResponse;
import org.springframework.beans.BeanUtils;

import java.util.Objects;

public class CompanyMapper {

    public static Company toRntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeeCount(Objects.isNull(company.getEmployees()) ? 0 : company.getEmployees().size());
        return companyResponse;
    }

}
