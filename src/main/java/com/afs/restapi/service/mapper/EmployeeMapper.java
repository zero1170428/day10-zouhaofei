package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.employee.EmployeeRequest;
import com.afs.restapi.service.dto.employee.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public EmployeeMapper() {
    }

    public static Employee toRntity(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest, employee);
        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }
}
