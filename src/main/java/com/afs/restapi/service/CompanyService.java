package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CompanyService {

    private final CompanyJPARepository companyJPARepository;
    private final EmployeeJPARepository employeeJPARepository;

    public CompanyService(CompanyJPARepository companyJPARepository, EmployeeJPARepository employeeJPARepository) {
        this.companyJPARepository = companyJPARepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public List<Company> findAll() {
        return companyJPARepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return companyJPARepository.findAll(pageRequest).getContent();
    }

    public Company findById(Long id) {
        Company company = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return company;
    }

    public Company update(Long id, Company company) {
        Company returnCompany = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        returnCompany.setName(company.getName());
        companyJPARepository.save(returnCompany);
        return findById(id);

    }

    public Company create(Company company) {
        return companyJPARepository.save(company);
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return employeeJPARepository.findByCompanyId(id);
    }

    @Transactional
    public void delete(Long id) {
        employeeJPARepository.deleteByCompanyId(id);
        companyJPARepository.deleteById(id);
    }
}
